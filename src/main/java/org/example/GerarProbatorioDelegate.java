package org.example;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import java.util.Random;

public class GerarProbatorioDelegate implements JavaDelegate {
    private DelegateExecution delegateExecution;

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        Random random = new Random();
        delegateExecution.setVariable("documentoId", "123455");
        delegateExecution.setVariable("documento", "yayyayayayay");
        delegateExecution.setVariable("probatorioOK", random.nextBoolean());
    }
}
